import React, { Component } from 'react'

export default class Home extends Component {
    render() {
        return (
            <div className="home-page-container">
                <h1>Core Tools</h1>
                <div>Homepage for Core Tools</div>
            </div>
        )
    }
}
