import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import {
    SET_POLY_DEGREE,
    INCREMENT_POLY_DEGREE,
    DECREMENT_POLY_DEGREE
} from 'actions/types'
import { SET_POLY_COEFFICIENTS } from '../actions/types'

const initialState = {
    coefficients: [-1, 2]
}

function poly(state = initialState, action) {
    switch (action.type) {
        case SET_POLY_DEGREE: {
            return {
                ...state,
                // You know whats going on right?
                coefficients: [
                    ...state.coefficients.filter((_, i) => i <= action.degree),
                    ...new Array(
                        Math.max(
                            action.degree + 1 - state.coefficients.length,
                            0
                        )
                    ).fill(1)
                ]
            }
        }
        case INCREMENT_POLY_DEGREE: {
            return {
                ...state,
                coefficients: [...state.coefficients, 1]
            }
        }
        case DECREMENT_POLY_DEGREE: {
            return {
                ...state,
                coefficients: [
                    ...state.coefficients.filter(
                        (_, i) => i < state.coefficients.length - 1
                    )
                ]
            }
        }
        case SET_POLY_COEFFICIENTS: {
            return {
                ...state,
                coefficients: action.coefficients
            }
        }
        default:
            return state
    }
}

const persistConfig = {
    key: 'poly',
    storage,
    blacklist: []
}

export const getCoefficients = state =>
    (state ? state.poly : initialState).coefficients || []

export default persistReducer(persistConfig, poly)
