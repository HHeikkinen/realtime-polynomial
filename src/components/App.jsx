import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from 'react-router-dom'
import { Helmet } from 'react-helmet'

// components
import Nav from './Header/Nav'
import Home from './Home'
import PolyPage from './PolyPage'

class App extends Component {
    componentDidMount() {}

    render() {
        return (
            <div>
                <Helmet
                    titleTemplate="%s - Core Tools"
                    defaultTitle="Core Tools"
                >
                    <meta
                        name="description"
                        content="Core Tools including Realtime Polynomials"
                    />
                </Helmet>
                <Switch>
                    <Route
                        path="/:route"
                        render={routeProps => {
                            const route = routeProps.match.params.route
                            return (
                                <div className="app-container">
                                    <Nav currentRoute={route} />
                                    <div className="app-content-container">
                                        <Switch>
                                            <Route
                                                path="/home"
                                                component={Home}
                                            />
                                            <Route
                                                path="/polynomials"
                                                component={PolyPage}
                                            />
                                            <Redirect to="/" />
                                        </Switch>
                                    </div>
                                </div>
                            )
                        }}
                    />
                    <Redirect to="/home" />
                </Switch>
            </div>
        )
    }
}

export default connect()(App)
