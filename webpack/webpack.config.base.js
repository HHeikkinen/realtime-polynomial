const path = require('path')
const webpack = require('webpack')
const Dotenv = require('dotenv-webpack')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = options => ({
    mode: options.mode,
    entry: options.entry,
    output: Object.assign(
        {
            path: path.resolve(process.cwd(), 'build'),
            publicPath: '/'
        },
        options.output
    ),
    optimization: options.optimization,

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: options.babelQuery
                }
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.(sass|scss)$/,
                exclude: /node_modules/,
                use: [
                    options.mode === 'development'
                        ? 'style-loader'
                        : MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /public\/.*\.(png|jpg|gif)$/i,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]'
                        }
                    }
                ]
            },
            {
                test: /assets\/.*\.(png|jpg|gif)$/i,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            fallback: 'file-loader',
                            limit: 8192
                        }
                    }
                ]
            },
            {
                test: /\.svg$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'url-loader'
                    }
                ]
            },
            {
                test: /(manifest\.webmanifest|browserconfig\.xml)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]'
                        }
                    },
                    {
                        loader: 'app-manifest-loader',
                        options: {
                            name: '[name].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.ttf$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/'
                        }
                    }
                ]
            }
        ]
    },
    target: 'node',
    plugins: options.plugins.concat([
        // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
        // inside your code for any environment checks; Terser will automatically
        // drop any unreachable code.
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV)
            }
        }),
        new Dotenv()
    ]),
    resolve: Object.assign(
        {
            alias: {
                actions: path.resolve(__dirname, '../src/actions'),
                reducers: path.resolve(__dirname, '../src/reducers'),
                assets: path.resolve(__dirname, '../src/assets'),
                constants: path.resolve(__dirname, '../src/constants.js')
            },
            extensions: ['.js', '.jsx']
        },
        options.resolve
    ),
    devtool: options.devtool,
    // control what bundle information gets displayed
    // https://webpack.js.org/configuration/stats/
    devServer: options.devServer,
    target: 'web', // Make web variables accessible to webpack, e.g. window
    performance: options.performance || {}
})
