import React, { Component } from 'react'

export default class Equation extends Component {
    renderCoefficient = (co, i) => (
        <EquationFragment
            key={i}
            coefficient={co}
            variable="x"
            degree={i}
            onChange={this.handleCoefficientChange}
        />
    )

    handleDegreeDecrement = () => {
        this.props.onChange(
            this.props.coefficients.filter(
                (_, i) => i === 0 || i < this.props.coefficients.length - 1
            )
        )
    }

    handleDegreeIncrement = () => {
        this.props.onChange([...this.props.coefficients, 1])
    }

    handleCoefficientChange = (value, degree) => {
        // console.log(value, degree)
        this.props.onChange([
            ...this.props.coefficients.slice(0, degree),
            value,
            ...this.props.coefficients.slice(degree + 1)
        ])
    }

    render() {
        const { coefficients } = this.props
        return (
            <div className="equation-component">
                <button onClick={this.handleDegreeDecrement} type="text">
                    -
                </button>
                <div className="equation-inputs">
                    {coefficients.map(this.renderCoefficient)}
                </div>
                <button onClick={this.handleDegreeIncrement} type="text">
                    +
                </button>
                {/* <button onClick={this.props.resetDegree} type='text'>Reset</button> */}
            </div>
        )
    }
}

class EquationFragment extends Component {
    handleCoefficientChange = e => {
        this.props.onChange(e.target.value, this.props.degree)
    }

    render() {
        const { coefficient, variable, degree } = this.props
        return (
            <div className="equation-fragment">
                <span>
                    {/* <span className='equation-element equation-coefficient'>{coefficient}</span> */}
                    <span className="equation-element equation-coefficient">
                        <input
                            value={coefficient}
                            onChange={this.handleCoefficientChange}
                        />
                    </span>
                    {degree >= 1 && (
                        <span className="equation-element equation-variable">
                            {variable}
                        </span>
                    )}
                    {degree >= 2 && (
                        <span className="equation-element equation-degree">
                            {degree}
                        </span>
                    )}
                </span>
            </div>
        )
    }
}
