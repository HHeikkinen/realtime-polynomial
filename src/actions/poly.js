import {
    SET_POLY_DEGREE,
    INCREMENT_POLY_DEGREE,
    DECREMENT_POLY_DEGREE,
    SET_POLY_COEFFICIENTS
} from './types'

export const setPolyDegree = degree => ({
    type: SET_POLY_DEGREE,
    degree
})

export const incrementPolyDegree = {
    type: INCREMENT_POLY_DEGREE
}

export const decrementPolyDegree = {
    type: DECREMENT_POLY_DEGREE
}

export const setPolyCoefficients = coefficients => ({
    type: SET_POLY_COEFFICIENTS,
    coefficients
})
