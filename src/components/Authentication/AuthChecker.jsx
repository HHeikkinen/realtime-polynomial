import React, { Component } from 'react'
import { connect } from 'react-redux'

class AuthCheck extends Component {
    render() {
        return this.props.children({
            isAuthenticated: this.props.authenticated
        })
    }
}

const mapStateToProps = state => ({
    authenticated: true
})

export default connect(mapStateToProps)(AuthCheck)
