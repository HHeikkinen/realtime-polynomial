import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getCoefficients } from 'reducers'
import { incrementPolyDegree, decrementPolyDegree } from 'actions'
import { setPolyDegree, setPolyCoefficients } from '../actions/poly'
import Equation from './Equation'

class PolyPage extends Component {
    handleEquationChange = coefficients => {
        this.props.setCoefficients(coefficients)
    }

    render() {
        const { coefficients } = this.props
        console.log(coefficients)
        return (
            <div className="poly-page">
                <h1>Realtime Polynomials</h1>
                <div className="poly-container"></div>
                <Equation
                    coefficients={coefficients}
                    onChange={this.handleEquationChange}
                />
            </div>
        )
    }
}

export default connect(
    state => ({
        coefficients: getCoefficients(state)
    }),
    dispatch => ({
        increment: () => dispatch(incrementPolyDegree),
        decrement: () => dispatch(decrementPolyDegree),
        resetDegree: () => dispatch(setPolyDegree(1)),
        setCoefficients: coefficients =>
            dispatch(setPolyCoefficients(coefficients))
    })
)(PolyPage)
